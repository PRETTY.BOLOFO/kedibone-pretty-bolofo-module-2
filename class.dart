void main() {
  var mtn = new Awards();
  mtn.app = "Augment Reality(AR)";
  mtn.category1 = "Best eduational solution";
  mtn.category2 = "Best South African solution";
  mtn.developer = "Ambani Afrika";
  mtn.year = 2021;
  mtn.printMtnAppAwardsWinnerInformation();
}

class Awards {

  String? app;
  String? developer;
  String? category1;
  String? category2;
  int? year;

  void printMtnAppAwardsWinnerInformation() {

    print("$app".toUpperCase());
    print("Categories $category1");
    print("Categories $category2");
    print("The developer is $developer");
    print("The year the app won is $year");
  }
  
}